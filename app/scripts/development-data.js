// Default Invodo afiliate config
window.INVODO_AFF_CONFIG = {
  'test': 'true',
  'rtmpBase': 'rtmp://aoaef.invodo.com/',
  'imageBase': 'http://e.invodo.com/media/',
  'httpBase': 'http://aoael.invodo.com/media/',
  'name': 'Marsha',
  'playbuttoncolor': '',
  'aspectratio': 'WIDESCREEN',
  'defaultquality': '',
  'bufferscreenbranding': 'INVODO',
  'endofcontentbranding': 'INVODO',
  'showcliptitles': 'false',
  'ratingenabled': 'true',
  'autoplaymultipleclips': 'true',
  'playlist': 'false',
  'watermark': 'false',
  'backcolor': '000000',
  'frontcolor': 'FFFFFF',
  'secondarycolor': 'c1267d',
  'share': 'true',
  'og': 'true'
};

// Set the Invodo affiliate
window.INVODO_AFF_CONFIG.affiliate = 'verabradley';

// Create a local IVP config for testing
window.IVP_DATA = {
  "video": {
    "autoplay": false,
    "duration": 600,
    "dimensions": {
      "width": 400,
      "height": 600,
      "unit": "px"
    }
  },
  "hotspot": {
    "enable": true,
    "templates": [
      {
        "text": "<span class=\"ivp-hotspot-icon\"></span><div class=\"ivp-hotspot-title\" role=\"button\" aria-live=\"polite\"><div><span>${text}</span></div></div>",
        "_id": "554d0a9fe9385609007497cb",
        "id": 0
      }
    ],
    "items": [
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 21
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "15306386",
          "text": "Quilted Stella Satchel"
        },
        "time": {
          "start": 61.5,
          "end": 66
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 69.7063369397218,
              "y": 40.38461538461539
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ec246b55b360900895e4b",
        "$$hashKey": "01W",
        "id": 0,
        "title": "15306386"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 20
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "14906081-1",
          "text": "Large Spinner"
        },
        "time": {
          "start": 4,
          "end": 6.9
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 9.73724884080371,
              "y": 38.73626373626374
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ec6bfb55b360900895e54",
        "$$hashKey": "01X",
        "id": 1,
        "title": "14906081-1"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 20
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "14906081-2",
          "text": "Large Spinner"
        },
        "time": {
          "start": 17.15,
          "end": 20.6
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 35.7032457496136,
              "y": 46.70329670329671
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ec6bfb55b360900895e54",
        "$$hashKey": "01Y",
        "id": 2,
        "title": "14906081-2"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 20
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "14906081-3",
          "text": "Large Spinner"
        },
        "time": {
          "start": 61.5,
          "end": 66
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 50.54095826893354,
              "y": 83.24175824175825
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ec6bfb55b360900895e54",
        "$$hashKey": "01Z",
        "id": 3,
        "title": "14906081-3"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 19
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "15382284",
          "text": "Smartphone Wristlet for iPhone 6"
        },
        "time": {
          "start": 35,
          "end": 38
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 42.658423493044815,
              "y": 81.31868131868131
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ec7c3b55b360900895e5d",
        "$$hashKey": "020",
        "id": 4,
        "title": "15382284"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 18
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "14981344-2",
          "text": "Hybrid Hardshell Case for iPhone 6"
        },
        "time": {
          "start": 35,
          "end": 38
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 50.85162287480679,
              "y": 20.87912087912088
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ec8b9b55b360900895e66",
        "$$hashKey": "021",
        "id": 5,
        "title": "14981344-2"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 17
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "14679393-1",
          "text": "Flip Fold Tablet Case"
        },
        "time": {
          "start": 31.8,
          "end": 34.65
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 29.520865533230296,
              "y": 21.428571428571427
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ec8e2b55b360900895e6f",
        "$$hashKey": "022",
        "id": 6,
        "title": "14679393-1"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 17
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "14679393-2",
          "text": "Flip Fold Tablet Case"
        },
        "time": {
          "start": 40.2,
          "end": 43.2
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 50.69551777434313,
              "y": 31.043956043956044
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ec8e2b55b360900895e6f",
        "$$hashKey": "023",
        "id": 7,
        "title": "14679393-2"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 16
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "12446081",
          "text": "Hanging Organizer"
        },
        "time": {
          "start": 10.75,
          "end": 15.5
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 25.50231839258114,
              "y": 56.59340659340659
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ec905b55b360900895e78",
        "$$hashKey": "024",
        "id": 8,
        "title": "12446081"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 15
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "14971344",
          "text": "Medium Cosmetic Case"
        },
        "time": {
          "start": 10.75,
          "end": 15.5
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 47.60432766615147,
              "y": 17.032967032967033
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ec92ab55b360900895e81",
        "$$hashKey": "025",
        "id": 9,
        "title": "14971344"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 14
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "14977081",
          "text": "Going Places Garment Bag"
        },
        "time": {
          "start": 22.5,
          "end": 25.35
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 40.80370942812983,
              "y": 55.769230769230774
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ec94ab55b360900895e8a",
        "$$hashKey": "026",
        "id": 10,
        "title": "14977081"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 12
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "15083236",
          "text": "Delicate Openwork Earrings"
        },
        "time": {
          "start": 27.7,
          "end": 30.5
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 67.59814528593508,
              "y": 56.86813186813187
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ec9dbb55b360900895e9c",
        "$$hashKey": "027",
        "id": 11,
        "title": "15083236"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 1
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "15095236",
          "text": "Sparkling Bar Bracelet"
        },
        "time": {
          "start": 26,
          "end": 27.6
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 63.833075734157646,
              "y": 75.54945054945054
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ecb71b55b360900895eff",
        "$$hashKey": "028",
        "id": 12,
        "title": "15095236"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 13
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "15417297",
          "text": "Kit Sunglasses"
        },
        "time": {
          "start": 31.8,
          "end": 34.65
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 72.79752704791345,
              "y": 47.527472527472526
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ec9b3b55b360900895e93",
        "$$hashKey": "029",
        "id": 13,
        "title": "15417297"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 11
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "15348344",
          "text": "Cozy Flannel Pajama Pant"
        },
        "time": {
          "start": 6.9,
          "end": 10.75
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 44.04945904173106,
              "y": 81.04395604395604
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561eca19b55b360900895ea5",
        "$$hashKey": "02A",
        "id": 14,
        "title": "15348344"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 10
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "15347713",
          "text": "Cozy Knit Pajama Top"
        },
        "time": {
          "start": 6.9,
          "end": 10.75
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 17.465224111282843,
              "y": 31.868131868131865
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561eca3eb55b360900895eae",
        "$$hashKey": "02B",
        "id": 15,
        "title": "15347713"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 9
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "15412284",
          "text": "Foxy Socks"
        },
        "time": {
          "start": 6.9,
          "end": 10.75
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 8.0370942812983,
              "y": 64.83516483516483
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561eca5fb55b360900895eb7",
        "$$hashKey": "02C",
        "id": 16,
        "title": "15412284"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 8
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "15345945",
          "text": "Cozy Slippers"
        },
        "time": {
          "start": 43.5,
          "end": 45.9
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 40.64914992272025,
              "y": 85.98901098901098
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561eca7ab55b360900895ec0",
        "$$hashKey": "02D",
        "id": 17,
        "title": "15345945"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 7
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "14246344",
          "text": "Hooded Fleece Robe"
        },
        "time": {
          "start": 50.7,
          "end": 59.7
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 21.792890262751165,
              "y": 20.32967032967033
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561eca9db55b360900895ec9",
        "$$hashKey": "02E",
        "id": 18,
        "title": "14246344"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 6
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "15341481236",
          "text": "Micro-Stud Knit Hat"
        },
        "time": {
          "start": 47.2,
          "end": 50.6
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 61.360123647604325,
              "y": 15.109890109890108
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ecac2b55b360900895ed2",
        "$$hashKey": "02F",
        "id": 19,
        "title": "15341481236"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 5
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "15335481",
          "text": "Micro-Stud Leather Gloves"
        },
        "time": {
          "start": 45.9,
          "end": 50.6
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 7.8825347758887165,
              "y": 70.05494505494505
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ecaeeb55b360900895edb",
        "$$hashKey": "02G",
        "id": 20,
        "title": "15335481"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 4
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "12408713",
          "text": "Throw Blanket"
        },
        "time": {
          "start": 10.75,
          "end": 15.5
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 76.8160741885626,
              "y": 71.7032967032967
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ecb10b55b360900895ee4",
        "$$hashKey": "02H",
        "id": 21,
        "title": "12408713"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 3
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "14792284",
          "text": "Travel Tumbler"
        },
        "time": {
          "start": 43.5,
          "end": 45.9
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 22.411128284389488,
              "y": 48.07692307692308
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ecb2db55b360900895eed",
        "$$hashKey": "02I",
        "id": 22,
        "title": "14792284"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 2
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "15337481236",
          "text": "Micro-Stud Ella Tote"
        },
        "time": {
          "start": 66.2,
          "end": 69.23
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 58.26893353941267,
              "y": 79.12087912087912
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ecb53b55b360900895ef6",
        "$$hashKey": "02J",
        "id": 23,
        "title": "15337481236"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 0
          }
        ],
        "templateId": "554d0a9fe9385609007497cb",
        "template": 0,
        "data": {
          "title": "Micro-Stud Audrey Wallet",
          "text": "Micro-Stud Audrey Wallet"
        },
        "time": {
          "start": 43.5,
          "end": 45.9
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 53.94126738794436,
              "y": 21.978021978021978
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "opacity": 0
          },
          "config": [
            {
              "opacity": 1
            },
            {
              "opacity": 0,
              "scale": 0.5
            }
          ]
        },
        "card": "561ecb8fb55b360900895f08",
        "$$hashKey": "02K",
        "id": 24,
        "title": "Micro-Stud Audrey Wallet"
      }
    ]
  },
  "card": {
    "enabled": true,
    "templates": [
      {
        "text": "<div class=\"ivp-card--medium\"><div class=\"ivp-card-content\"><div class=\"ivp-container\"><div class=\"ivp-row\"><div class=\"ivp-col-12 ivp-col-pull-gutters ivp-card-image\"><img src=\"${image}\"></div></div><div class=\"ivp-row\"><div class=\"ivp-col-12 ivp-card-text\"><h3>${title}</h3><p class=\"ivp-card-price\">${price}</p><div class=\"ivp-card-ctas\"><a class=\"ivp-card-cta ivp-card-cta--primary\" href=\"${ctaprimarylink}\" target=\"_blank\">${ctaprimarytext}</a><a class=\"ivp-card-cta ivp-card-cta--secondary\" href=\"${ctasecondarylink}\" target=\"_blank\">${ctasecondarytext}</a></div><p>${text}</p></div></div></div></div></div>",
        "_id": "5543d84d50f66a0900fd1494",
        "id": 0
      }
    ],
    "items": [
      {
        "_id": "561ecb8fb55b360900895f08",
        "title": "Micro-Stud Audrey Wallet",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/15338481236.png",
          "title": "Micro-Stud Audrey Wallet",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/micro-stud-audrey-wallet/black-with-gold-tone/1004119_203761.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "Inside this pin snap closure wallet there are two compartments that contain a total of 12 credit card slips, two large bill pockets and two zip pockets."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 0,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ecb71b55b360900895eff",
        "title": "VB-15095236",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/15095236.png",
          "title": "Sparkling Bar Bracelet",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/sparkling-bar-bracelet/gold-tone/1003923_201570.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "Pavé details make this delicate bangle sparkle and shine."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 1,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ecb53b55b360900895ef6",
        "title": "VB-15337481236",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/15337481236.png",
          "title": "Micro-Stud Ella Tote",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/micro-stud-ella-tote/black-with-gold-tone/1004115_203760.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "Two large slips and a zip pocket provide organization under the breakaway zip top and a hidden magnetic closure pocket at the top seam keeps keys or a phone close by."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 2,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ecb2db55b360900895eed",
        "title": "VB-14792284",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/14792284.png",
          "title": "Travel Tumbler",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/travel-tumbler/alpine-floral/1003829_203347.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "We made our favorite take-along tumbler even more perfect with a new flexible straw. Featuring one of our pretty prints, it includes a screw-on lid and is BPA-free."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 3,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ecb10b55b360900895ee4",
        "title": "VB-12408713",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/12408713.png",
          "title": "Throw Blanket",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/throw-blanket/zebra/1001706_203080.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "The Throw Blanket is generously sized and so soft. The 100% polyester micro-fleece is washable and lightweight."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 4,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ecaeeb55b360900895edb",
        "title": "VB-15335481",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/15335481M-L.png",
          "title": "Micro-Stud Leather Gloves",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/micro-stud-leather-gloves/black/1004163_203804.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "Tech friendly fabric and leather gloves are accented with on trend micro studs for stylish warmth this winter. Thumb and first finger are tech friendly."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 5,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ecac2b55b360900895ed2",
        "title": "VB-15341481236",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/15341481236.png",
          "title": "Micro-Stud Knit Hat",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/micro-stud-knit-hat/black-with-gold-tone/1004162_203764.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "This cozy hat is accented with micro studs so you can stay warm and stylish this winter."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 6,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561eca9db55b360900895ec9",
        "title": "VB-14246344",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/14246344L-XL.png",
          "title": "Hooded Fleece Robe",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/hooded-fleece-robe/rosewood/1003587_203802.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "We added a cozy hood to our 100% microfleece robe. This supersoft robe has convenient patch pockets and a tie belt for an always perfect fit."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 7,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561eca7ab55b360900895ec0",
        "title": "VB-15345945",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/15345945L.png",
          "title": "Cozy Slippers",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/cozy-slippers/light-blue/1004160_203813.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "Solid suedette slippers are accented with a cozy printed fleece lining. They also have a hard sole and a cushioned foam footbed."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 8,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561eca5fb55b360900895eb7",
        "title": "VB-15412284",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/15412284.png",
          "title": "Foxy Socks",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/foxy-socks/alpine-floral/1004159_203664.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "Your toes should look as good as your bag and they will when you wear Foxy Socks! Woven patterns drawn from our Signature prints will keep you warm and and in style."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 9,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561eca3eb55b360900895eae",
        "title": "VB-15347713",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/15347713L.png",
          "title": "Cozy Knit Pajama Top",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/cozy-knit-pajama-top/zebra/1004164_203835.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "This cozy long-sleeved knit pajama top coordinates with our Cozy Flannel Pajama Pants. You'll be cozy and cute this winter!"
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 10,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561eca19b55b360900895ea5",
        "title": "VB-15348344",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/15348344L.png",
          "title": "Cozy Flannel Pajama Pant",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/cozy-flannel-pajama-pant/rosewood/1004153_203853.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "Cuddle up in these soft-brushed flannel pajama pants. They feature side pockets, a drawstring, an elastic waist and piping detail."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 11,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ec9dbb55b360900895e9c",
        "title": "VB-15083236",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/15083236.png",
          "title": "Delicate Openwork Earrings",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/delicate-openwork-earrings/gold-tone/1003918_201561.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "Delicate filigree disks dangle from these feminine earrings.Earring posts are nickel safe."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 12,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ec9b3b55b360900895e93",
        "title": "VB-15417297",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/15417297.png",
          "title": "Kit Sunglasses",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/kit-sunglasses/marrakesh/1003856_201465.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "These classic pilot aviators sport a splash of Vera Bradley color. Includes a drawstring fabric case that doubles as a cleaning cloth."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 13,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ec94ab55b360900895e8a",
        "title": "VB-14977081",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/14977081.png",
          "title": "Going Places Garment Bag",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/going-places-garment-bag/classic-black/1004048_202649.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "The Going Places Garment bag is perfect to keep hanging clothes neat and clean while traveling. An exterior pocket keeps small accessories handy."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 14,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ec92ab55b360900895e81",
        "title": "VB-14971344",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/14971344.png",
          "title": "Medium Cosmetic Case",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/medium-zip-cosmetic-bag/rosewood/1003899_203426.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "This classic zip-top cosmetic bag is perfect for travel or on a vanity table. The material easily wipes clean."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 15,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ec905b55b360900895e78",
        "title": "VB-12446081",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/12446081.png",
          "title": "Hanging Organizer",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/hanging-travel-organizer/classic-black/1001755_176934.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "Lotions, powders and sprays all have a home here, thanks to the roomy, gusseted bottom pocket. It's ideal for slipping things into your suitcase or hanging it near the vanity."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 16,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ec8e2b55b360900895e6f",
        "title": "VB-14679393",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/14679393.png",
          "title": "Flip Fold Tablet Case",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/flip-fold-tablet-case/black-white-studs/1003846_203343.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "This case has both style and function. The back of the case snaps onto the tablet while the magnetic cover folds back into an easel for hands free use."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 17,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ec8b9b55b360900895e66",
        "title": "VB-14981344",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/14981344.png",
          "title": "Hybrid Hardshell Case for iPhone 6",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/hybrid-hardshell-phone-case-for-iphone-6/rosewood/1003891_203438.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "This colorful case offers a premium polycarbonate frame and a rubber surround that, together, provide great protection. It’s a perfect fit for the iPhone 6."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 18,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ec7c3b55b360900895e5d",
        "title": "VB-15382284",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/15382284.png",
          "title": "Smartphone Wristlet for iPhone 6",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/smartphone-wristlet-for-iphone-6/alpine-floral/1004182_203626.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "This now holds an iPhone 6! Inside, there are numerous card slips, a bill compartment and an ID window, plus two slip pockets. The exterior also features a zip coin compartment."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 19,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ec6bfb55b360900895e54",
        "title": "VB-14906081",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/14906081.png",
          "title": "27\" Spinner",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/large-spinner-rolling-luggage/classic-black/1003894_201893.uts?N=50005",
          "ctasecondarytext": "SHOP NOW",
          "text": "This much-loved travel piece is lightweight and features swivel wheels and a telescoping handle. Plus, this travel companion has outside zip pockets and an adjustable compression straps inside."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 20,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      },
      {
        "_id": "561ec246b55b360900895e4b",
        "title": "VB-15306386",
        "template": 0,
        "data": {
          "image": "//ixd.invodo.com/ivp-experiences/ivp-customer-verabradley/images/15306386.png",
          "title": "Quilted Stella Satchel",
          "price": "",
          "ctaprimarylink": "",
          "ctaprimarytext": "ADD TO BAG",
          "ctasecondarylink": "http://www.verabradley.com/product/quilted-stella-satchel/aubergine/1004116_203540.uts",
          "ctasecondarytext": "SHOP NOW",
          "text": "This silhouette is offered in a quilted leather construction, the pushlock closure accents this stylish satchel. Carry as a hand-held bag or use the detachable strap and carry as a shoulder bag."
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "opacity": 0,
            "top": "50%",
            "right": 0
          },
          "config": [
            {
              "y": "-50%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "-54%"
            }
          ]
        },
        "id": 21,
        "position": {
          "x": "0",
          "y": "-54%"
        }
      }
    ]
  },
  "poster": {
    "templates": [
      {
        "id": 1,
        "text": "<div class=\"ivp-btn ivp-btn--big-play\" role=\"button\" aria-live=\"polite\" tabindex=\"0\"><div><span>Play</span></div></div>"
      }
    ],
    "items": [
      {
        "id": 1,
        "enabled": true,
        "template": 1,
        "data": {
          "image": ""
        }
      }
    ]
  }
};
